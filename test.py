import unittest
import sys
import logging
import wsbasic_client

logging.basicConfig(level=logging.DEBUG)

user = None
password = None
app_id = None
app_secret = None
device_id = 'optional'
device_id_2 = 'optional'
token = None

class TestWSBasicClient(unittest.TestCase):
  def test_login(self):
    global token
    client = wsbasic_client.WSBasicClient.get_instance(wsbasic_client.WSBasicClient.WS_BASIC_URL_TEST)
    token = client.login(user, password, app_id, app_secret, device_id, device_id_2)
    logging.info("token: " + token)
    self.assertTrue(token)

  def test_validate(self):
    client = wsbasic_client.WSBasicClient.get_instance(wsbasic_client.WSBasicClient.WS_BASIC_URL_TEST)
    token_new = client.validate(token, app_id, app_secret, device_id, device_id_2)
    logging.info("token.1: " + token_new)
    self.assertTrue(token_new)
    token_new = client.validate(token_new, app_id, app_secret, device_id, device_id_2)
    logging.info("token.2: " + token_new)
    self.assertTrue(token_new)
    token_new = client.validate(token_new, app_id, app_secret, device_id, device_id_2)
    logging.info("token.3: " + token_new)
    self.assertTrue(token_new)
    token_new = client.validate(token_new, app_id, app_secret, device_id, device_id_2)
    logging.info("token.4: " + token_new)
    self.assertTrue(token_new)

if __name__ == '__main__':
  if len(sys.argv) < 5:
    print "usage: " + sys.argv[0] + " user_id password app_id app_secret"
    exit()

  user = sys.argv[1]
  password = sys.argv[2]
  app_id = sys.argv[3]
  app_secret = sys.argv[4]
  sys.argv = sys.argv[:1]
  unittest.main()
