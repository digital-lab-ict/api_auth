import os
import urllib2
import logging
import hashlib
import datetime
import json

class WSBasicClient:
    def __init__(self, url):
        self._url = url

    WS_BASIC_URL_TEST = "https://st-wsbasic.eni.it/EniWSSO/"
    WS_BASIC_URL_PROD = "https://wsbasic.eni.it/EniWSSO/"

    OPERATION_AUTHENTICATE = "authenticate"
    OPERATION_TOKENVALID = "tokenvalid"

    clients = {}

    @classmethod
    def get_instance(cls, url=None):
        if not url:
            url = cls.WS_BASIC_URL_PROD if os.environ['API_ENV'] == 'PROD' else cls.WS_BASIC_URL_TEST
        cli = cls.clients.get(url)
        if cli is None:
            cli = WSBasicClient(url)
            cls.clients[url] = cli
        return cli

    @classmethod
    def calc_signature(cls, salt, user_id, password, operation, device_id, secondarydeviceid, ts, idapp, tokenid ):
        return hashlib.sha256(salt + user_id + password + operation + device_id + secondarydeviceid + ts + idapp + tokenid).hexdigest()

    def login(self, user_id, password, app_id, app_secret, device_id, device_id_2):
        operation = self.OPERATION_AUTHENTICATE
        ts = datetime.datetime.strftime(datetime.datetime.utcnow(), '%d%m%Y%H%M%S')
        sig = self.calc_signature(salt=app_secret,
                                  user_id=user_id,
                                  password=password,
                                  operation=operation,
                                  device_id=device_id,
                                  secondarydeviceid=device_id_2,
                                  ts=ts,
                                  idapp=app_id,
                                  tokenid='')
        data = {'user': user_id,
                'password': password,
                'operation': operation,
                'deviceid': device_id,
                'secondarydeviceid': device_id_2,
                'idapp': app_id,
                'timestamp': ts,
                'signature': sig }
        data_json = json.dumps(data)
        req = urllib2.Request(self._url + 'ServiceLoginRest', data_json)
        req.add_header('Content-Type', 'application/json')
        req.add_header('Accept', 'application/json')
        try:
          handle = urllib2.urlopen(req)
          if handle.getcode() != 200:
            raise IOError("HTTP error: " + str(handle.getcode()))
          data = json.load(handle)
          response = data.get('response')
          return response.get('TOKEN')
        except:
      	  raise
        return None

    def validate(self, token, app_id, app_secret, device_id, device_id_2):
        operation = self.OPERATION_TOKENVALID
        ts = datetime.datetime.strftime(datetime.datetime.utcnow(), '%d%m%Y%H%M%S')
        sig = self.calc_signature(salt=app_secret,
                                  user_id='',
                                  password='',
                                  operation=operation,
                                  device_id=device_id,
                                  secondarydeviceid=device_id_2,
                                  ts=ts,
                                  idapp=app_id,
                                  tokenid=token)
        data = {'tokenid': token,
                'operation': operation,
                'deviceid': device_id,
                'secondarydeviceid': device_id_2,
                'idapp': app_id,
                'timestamp': ts,
                'signature': sig }
        data_json = json.dumps(data)
        req = urllib2.Request(self._url + 'ServiceLoginRest', data_json)
        req.add_header('Content-Type', 'application/json')
        req.add_header('Accept', 'application/json')
        try:
          handle = urllib2.urlopen(req)
          if handle.getcode() != 200:
            raise IOError("HTTP error: " + str(handle.getcode()))
          data = json.load(handle)
          response = data.get('response')
          token_new = response.get('TOKEN') if len(response.get('TOKEN')) else token
          return token_new
        except:
      	  raise
        return None
