import io
import os
import json
import logging
import urllib2
import time
from jose import jwt

from flask import Flask, request
from flask.ext.cors import CORS

import wsbasic_client

logging.basicConfig(level=logging.DEBUG)

app = Flask(__name__)
CORS(app)

HMAC_SHARED_SECRET = None

@app.route("/")
def hello():
  return "api server v1.0"

@app.route("/user-auth/1.0/auth/login", methods=['POST'])
def login():
    logging.info("login.1")
    ret_val = {"status": "ko", 'error': 'invalid_request'}
    try:
        input = request.get_data()
        data = json.loads(input)
        wsauth_client = wsbasic_client.WSBasicClient.get_instance()
        sm_token = wsauth_client.login(data.get("user"),
                                     data.get("password"),
                                     data.get("app_id"),
                                     data.get("app_secret"),
                                     data.get("device_id", "optional"),
                                     data.get("device_id_2", "optional"))

        if sm_token:
            jw_token = jwt.encode({'sub': data.get('key'),
                                   'user_id': data.get("user"),
                                   'exp': int(time.time() + 1800000),
                                   'sm_token': sm_token,
                                   'app_id': data.get("app_id"),
                                   'app_secret': data.get("app_secret"),
                                   'device_id': data.get("device_id", "optional"),
                                   'device_id_2': data.get("device_id_2", "optional")}, HMAC_SHARED_SECRET, algorithm='HS256')

            ret_val = {"status": "ok", 'jwt': jw_token}
    except Exception as e:
        logging.info("Exception: " + str(e))
    return json.dumps(ret_val)

@app.route("/user-auth/1.0/auth/login/smtoken", methods=['POST'])
def login_smtoken():
    logging.info("login.smtoken.1")
    ret_val = {"status": "ko", 'error': 'invalid_request'}
    try:
        input = request.get_data()
        data = json.loads(input)

        wsauth_client = wsbasic_client.WSBasicClient.get_instance()
        sm_token = wsauth_client.validate(data.get("sm_token"),
                                          data.get("app_id"),
                                          data.get("app_secret"),
                                          data.get("device_id", "optional"),
                                          data.get("device_id_2", "optional"))
        if sm_token:
            jw_token = jwt.encode({'sub': data.get('key'),
                                   'user_id': data.get("user"),
                                   'exp': int(time.time() + 1800000),
                                   'sm_token': sm_token,
                                   'app_id': data.get("app_id"),
                                   'app_secret': data.get("app_secret"),
                                   'device_id': data.get("device_id", "optional"),
                                   'device_id_2': data.get("device_id_2", "optional")}, HMAC_SHARED_SECRET, algorithm='HS256')

            ret_val = {"status": "ok", 'jwt': jw_token}
    except Exception as e:
        logging.info("Exception: " + str(e))
    return json.dumps(ret_val)

@app.route("/user-auth/1.0/auth/validate", methods=['GET'])
def validate():
    logging.info("validate.1")
    ret_val = {"status": "ko", 'message': 'token_not_valid'}
    try:
        token = request.headers.get('Authorization')
        json_token = jwt.decode(token, HMAC_SHARED_SECRET, algorithms=['HS256'])
        if json_token and json_token.get('exp') > int(time.time()):
            sm_token = json_token.get("sm_token")
            wsauth_client = wsbasic_client.WSBasicClient.get_instance()
            sm_token = wsauth_client.validate(json_token.get("sm_token"),
                                              json_token.get("app_id"),
                                              json_token.get("app_secret"),
                                              json_token.get("device_id", "optional"),
                                              json_token.get("device_id_2", "optional"))
            if sm_token:
                jw_token = jwt.encode({'sub': json_token.get('sub'),
                                       'user_id': json_token.get("user_id"),
                                       'exp': int(time.time() + 1800000),
                                       'sm_token': sm_token,
                                       'app_id': json_token.get("app_id"),
                                       'app_secret': json_token.get("app_secret"),
                                       'device_id': json_token.get("device_id", "optional"),
                                       'device_id_2': json_token.get("device_id_2", "optional")}, HMAC_SHARED_SECRET, algorithm='HS256')
                ret_val = {"status": "ok", 'message': 'token_valid', 'jwt': jw_token}
    except Exception as e:
        logging.info("Exception: " + str(e))
    return json.dumps(ret_val)

@app.route("/swagger")
def test():
  return app.send_static_file('api_auth_swagger.yaml')

if __name__ == "__main__":
  HMAC_SHARED_SECRET = os.environ['API_JWT_SECRET']
  app.run(host='0.0.0.0',port=5000,threaded=True)
